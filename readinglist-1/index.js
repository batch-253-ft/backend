console.log("Reading list #1");

/*
Functions
https://developer.mozilla.org/en-US/docs/Web/JavaScript/Guide/Functions

if-else Statement
https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Statements/if...else

switch Statement
https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Statements/switch

Create a simple Shipping Function for an online store that will use functions, if-else and switch statements
Features:
	1. Calculates shipping price via the item's weight. (use if-else)
		less than half a kilo is P90
		more than half a kilo and less than or equal to a kilo is P120
		more than a kilo and less than or equal to 3 kilos is P250
		more than 3 kilos and less than or equal to 5 kilos is P380
		more than 5 kilos and less than or equal to 10 kilos is P550
		return a message that will display more than 10 kilos must not be allowed 
	2. Can select a shipping method from local or overseas (use switch)
		multiply by 1.0 the shipping fee when local is selected
		multiply by 1.5 the shipping fee when overseas is selected
	3. Return a message that will inform user for the total amount of shipping fee
	4. Invoke the function in the console.

*/

let shippingPrice;

function calculateShippingPrice(weight){
	if(weight<0.5){
		shippingPrice = 90;
	} else if(weight>0.5 && weight<=1){
		shippingPrice = 120;
	} else if(weight>1 && weight<=3){
		shippingPrice = 250;
	} else if(weight>3 && weight<=5){
		shippingPrice = 380;
	} else if(weight>5 && weight<=10){
		shippingPrice = 550;
	} else if (weight>10){
		return "More than 10 kilos is not allowed. Book grabcar!"
	} else {
		return "Input weight of Parcel."
	}
}



function shippingMethod(method){
	switch (method){
		case "local":
			shippingPrice = parseInt(shippingPrice*1);
			return `Total cost of shipping is ${shippingPrice}`
			break;

		case "overseas":
			shippingPrice = parseInt(shippingPrice*1.5);
			return `Total cost of shipping is ${shippingPrice}`
			break;
		default:
			return `Please choose a shipping method!`
	}

};

