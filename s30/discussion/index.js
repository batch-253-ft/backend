db.fruits.insertMany([
	{
		name : "Apple",
		color : "Red",
		stock : 20,
		price: 40,
		supplier_id : 1,
		onSale : true,
		origin: [ "Philippines", "US" ]
	},

	{
		name : "Banana",
		color : "Yellow",
		stock : 15,
		price: 20,
		supplier_id : 2,
		onSale : true,
		origin: [ "Philippines", "Ecuador" ]
	},

	{
		name : "Kiwi",
		color : "Green",
		stock : 25,
		price: 50,
		supplier_id : 1,
		onSale : true,
		origin: [ "US", "China" ]
	},

	{
		name : "Mango",
		color : "Yellow",
		stock : 10,
		price: 120,
		supplier_id : 2,
		onSale : false,
		origin: [ "Philippines", "India" ]
	}
]);

// MongoDB Aggregation Method
// Used to generate manipulated data and perform operations to create filtered results that helps in analyzing data


// Single purpose aggregation operations
// If we only need simple aggregations with only 1 stage
db.fruits.count();

/*
	- The "$match" is used to pass the documents that meet the specified conditions
		Syntax: { $match: {"field": "value"}}

	- the "$group" is used to group the elements together and field-value pairs using the data from the grouped elements
		Syntax: { $group: {"_id": "value", "fieldResult": "valueResult"}}
*/

// Pipelines with multiple stages
db.fruits.aggregate(
	{$match: {
		"onSale": true
	}},
	{$group: {
		"_id": "$supplier_id", //grouped by supplier_id
		"total": {$sum: "$stock"} // total values of all stocks
	}}
);

// Field projection with aggregation
// $project can be used aggregating data to include/exclude fields from the returned results
/*
	Syntax:
		{$project: {"field": 1/0}}

*/

db.fruits.aggregate([
	{ 
		$match: {"onSale": true}
	},
	{ 
		$group: {
			"_id": "$supplier_id", "total": { 
			$sum: "$stock"}
		}
	},
	{ 
		$project: {"_id": 0}
	}

]);

// Mini activity
/*
	Aggregate all fruits that has a stock greater than or equal to 20
	group by supplier id show the sum of all stock

*/


db.fruits.aggregate([
	{ 
		$match: {"stock": {$gte: 20}}
	},
	{ 
		$group: {
			"_id": "$supplier_id", 
			"total": { $sum: "$stock"}
		}
	},
	{ 
		$project: {"_id": 0}
	}

]);

// Sorting aggregated results
/*
	- The "$sort" can be used to change the order of aggregated results
	- Providing a value of -1 will sort the aggregated results in a reverse order
	Syntax:
		{ $sort: {"fieldToBeSorted": 1/-1}} 

*/
db.fruits.aggregate([
	{
		$match: {"onSale": true}
	},
	{
		$group: {
			"_id": "$supplier_id",
			"total": {$sum: "$stock"}
		}
	},
	{
		$sort: {"total": -1}
	}
]);
// Computation for aggregated results. Mostly used in $group staging
/*
	$sum - to total the values
	$avg - to average the values
	$max - maximum value
	$min - minimum value

	Syntax:
	$group: {
		"_id": "$fieldToBeGroup",
		"fieldForResults": {$sum/avg/max/min: "$fieldToGetResults"}
	}

*/
// Aggregating results based on array fields
/*
	- The "$unwind" deconstructs an array field from a collection/field with an array value to output a result for each element.

	Syntax:
	{$unwind: "$field"}


*/

db.fruits.aggregate([
	{$unwind: "$origin"}
]);


db.fruits.aggregate([
	{
		$unwind: "$origin"
	},
	{
		$group: {
			"_id": "$origin",
			"kinds": {$sum: 1}
		}
	}
]);

// ========= Schema Design ==========

let owner = ObjectId();

db.owners.insertOne({
	"_id": owner,
	"name": "John Smith",
	"contact": "09123456789"
});

// Referenced Data Model
db.suppliers.insertOne({
	"name": "ABC fruits",
	"contact": "09987654321",
	"owner_id": <owner_id>
});

db.suppliers.insertOne({
	"name": "DEF Fruits",
	"contact": "09321456987"
	"address": [
		{
			"street": "123 San Jose St.",
			"city": "Manila"
		},
		{
			"street": "987 Gil Puyat St.",
			"city": "Makati"
		}

	]
});
