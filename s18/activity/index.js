// console.log("Hello!");

function addNum(numA, numB) {
	
	return("Displayed sum of " + numA + " and " + numB);
	return(numA+numB);
};



function subNum(numC, numD) {
	
	return("Displayed difference of " + numC + " and " + numD);
	return(numC-numD);
};

addNum(5,15);
subNum(20,5);



function multiplyNum(numE, numF) {
	return("The product of " + numE + " and " + numF);
	return numE * numF; 
};

function divideNum(numE, numF) {
	return("The quotient of " + numE + " and " + numF);
	return numE / numF; 
};

let product = multiplyNum(50,10);
return(product);

let quotient = divideNum(50,10);
return(quotient);

function getCircleArea(radius) {
	return("The result of getting the area of a circle with " + radius + " radius :");
	return ((3.1416)*(radius)**2);

};
let circleArea = getCircleArea(15);
return(circleArea);

function getAverage(num1,num2,num3,num4){
	return("The average of " + num1 + ", " + num2 + " , " + num3 + " and " + num4 + " :");
	return ( (num1+num2+num3+num4)/4);
};

let averageVar = getAverage(20,40,60,80);
return(averageVar);


function checkIfPassed(num5,num6) {
	return("Is " + num5 + "/" + num6 + " a passing score?");
	let percentage = (num5/num6)*100;
	let isPassed = percentage >= 75;
	return isPassed;

};
let isPassingScore = checkIfPassed(38,50);
return(isPassingScore);








//Do not modify
//For exporting to test.js
try {
	module.exports = {
		addNum,subNum,multiplyNum,divideNum,getCircleArea,getAverage,checkIfPassed
	}
} catch (err) {

}