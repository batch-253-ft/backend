// console.log("Hello cutie yieeee")

// LOOPS
	// While Loop
	/*
		- A while loop takes in an expression/condition
		- Expressions are any unit of code that can be evaluated to a value
		- If the condition evaluates to true, the statements inside the code block will be executed
		- A statement is a command that the programmer gives to the computer
		- A loop will iterate a certain number of times until an expression/condition is met
		- "Iteration" is the term given to the repetition of statements


		Syntax:
			while(expression/condition){
				statement;
			}
	*/

// while the value of count is not equal to 0
let count = 5;

while (count !== 0){

	// the current value of count is printed out
	console.log("While: " + count);
	count--;
	console.log(count);
}


// let i = 1
// while(i <= 3){
// 	console.log("Hi " + i);
// 	i++;
// }

// Do-while loop

/*
	- A do-while loop works a lot like the while loop. But unlike while loops, do-while loops guarantee that the code will be executed at least once.
	
	Syntax:
		do{
			statement

		} while(expression/condition)

*/

/*let number =Number(prompt("Give me a number"));

do{
	// the current value of a number is printed out
	console.log("Do while: " + number);
	// increases the value of number by 1 after every iteration to stop the loop when it reaches 10 or greater
	// number = number+1 (varName += num - reassigning value by adding "num" value)
	number += 1
} while (number < 10);*/


// For loop
/*
	
	- A for loop is more flexible than while and do-while loops. It consists of three parts:
	    1. The "initialization" value that will track the progression of the loop.
	    2.  The "expression/condition" that will be evaluated which will determine whether the loop will run one more time.
	    3. The "finalExpression" indicates how to advance the loop.
	
	Syntax:
		for(initialization/initial value; expression/condition; iteration){
			Statement;
		}

*/
/*
    - Will create a loop that will start from 0 and end at 20
    - Every iteration of the loop, the value of count will be checked if it is equal or less than 20
    - If the value of count is less than or equal to 20 the statement inside of the loop will execute
    - The value of count will be incremented by one for each iteration
*/
// for (let n = 0; n <= 5; n++) {
// 	// The current value of n is printed out
// 	console.log(n);
// }

let myString = "IAmADeveloper";
console.log(myString.length);

// console.log(myString[0]);
// console.log(myString[1]);
// console.log(myString[2]);
// console.log(myString[3]);


for (let x = 0; x < myString.length; x++){
	console.log(myString[x]);
}

let givenName = "SaUlDARiUs";
let vowels = 1;
	// for(let x = 0; x <givenName.length; x++){
	// 	if(
	// 		givenName[x] == "a" ||
	// 		givenName[x] == "e" ||
	// 		givenName[x] == "i" ||
	// 		givenName[x] == "o" ||
	// 		givenName[x] == "u" 
	// 		){
	// 		console.log(vowels++);
	// 	}
	// }
	
for(let i = 0; i < givenName.length; i++){
	if(
		// // If the character of your name is a vowel letter, instead of displaying the character, display "*"
		    // The ".toLowerCase()" function/method will change the current letter being evaluated in a loop to a lowercase letter ensuring that the letters provided in the expressions below will match
		givenName[i].toLowerCase() == "a" ||
		givenName[i].toLowerCase() == "e" ||
		givenName[i].toLowerCase() == "i" ||
		givenName[i].toLowerCase() == "o" ||
		givenName[i].toLowerCase()  == "u" 
		){

		// If the letter in the name is a vowel, it will print the *
		console.log("*");
	} else{

		// Print in the console all non-vowel characters in the name
		console.log(givenName[i]);
	}
}

for (let count = 0; count <= 20; count++){
	if(count%2 === 0){

		continue;
	}
	console.log("Continue and Break: " + count)

	if(count>10){
		break;
	}
}

let name = "alejandro";

for (let i = 0; i < name.length; i++){

	console.log(name[i]);

	if (name[i].toLowerCase()=== "a"){
		console.log("Continue to the next iteration");
		continue;
	}

	if(name[i] == "d"){
		break;
	}
}