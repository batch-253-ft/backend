// SETUP dependencies
const express = require("express");
const mongoose = require("mongoose");

// This allows us to use all the routes defined in "taskRoute.js"
const taskRoute = require("./routes/taskRoute");

// server setup
const app = express();
const port = 4000;
app.use(express.json());
app.use(express.urlencoded({ extended: true}));

mongoose.connect("mongodb+srv://admin:admin123@b253-hernandez.zmuemci.mongodb.net/s36?retryWrites=true&w=majority", {
	useNewUrlParser: true,
	useUnifiedTopology: true
});

app.use("/tasks", taskRoute);

let db = mongoose.connection; 
 
db.on("error", console.error.bind(console, "connection error"));


db.once("open", () => console.log("We're connected to the cloud database"));









if(require.main === module){
	app.listen(port, () => console.log(`Listening at port ${port}...`));
}

module.exports = app;