// console.log("Haruu");

// Javascript Synchronus vs Asynchronous
// Javascript is by default is synchronous meaning that only one statement is executed at a time


console.log("Hello World");
// conosole.log("Hello World");
console.log("Guttem Morgen");

// console.log("Hello World");
// for(let i = 0; i <=100; i++){
// 	console.log(i);

// };
console.log("Hello again");


// Asynchronous means that we can proceed to execute other statements, while time consuming code is running in the background

// Getting All posts
// The Fetch API allows you to asynchronously request for a resource (data)
// A "promise" is an object that represents the eventual completion (or failure) of an asynchronous function and it's resulting value
/*
	Syntax:
		fetch("url")

*/

console.log(fetch("https://jsonplaceholder.typicode.com/posts"));

/*
	Syntax:
		fetch("url")
			.then((response))
*/

// Retrieves all posts following the Rest API (retrieve, /posts, GET)
// By using the then method we can now check for the status of the promise
fetch("https://jsonplaceholder.typicode.com/posts")
// The "fetch" method will return a "promise" that resolves to a "Response" object
// The "then" method captures the "Reponse" object and returns another "promise" which will eventually be "resolved" or "rejected"
	.then(response => {
		console.log(response.status)
	});

fetch("https://jsonplaceholder.typicode.com/posts")
// Use the "json" method from the "Response" object to convert the data retrieved into JSON format to be used in our application
	.then(response => response.json()
		// Print the converted JSON value from the "fetch" request
		// Using multiple "then" methods creates a "promise chain"
	).then((json) => console.log(json));

fetch("https://jsonplaceholder.typicode.com/posts")
	.then((response) => {
		return response.json()
	}).then((json) => console.log(json));

// The "async" and "await" keywords is another approach that can be used to achieve asynchronous code
// Used in functions to indicate which portions of code should be waited for

// Creates an asynchronous function
async function fetchData(){

	// waits for the "fetch" method to complete then stores the value in the "result" value
	let result = await fetch("https://jsonplaceholder.typicode.com/posts");

	// Result returned by fetch returns a promise
	console.log(result)
	// The returned "Response" is an object
	console.log(typeof result);
	// We cannot access the content of the "Response" by directly accessing it's body property
	console.log(result.body);
	// Converts the data from the "Response" object as JSON
	let json = await(result.json());
	// Print out the content of the "Response" object
	console.log(json);
}

fetchData();

// Getting specific post

// Retrieves a specific post following the Rest API (retrieve, /posts/:id, GET)
fetch("https://jsonplaceholder.typicode.com/posts/1")
	.then(response => response.json())
	.then(json => console.log(json));

// Creating a post
/*
	Syntax:
		fetch("url", {options})
			.then((response) => {})
			.then((response) => ())

*/
fetch("https://jsonplaceholder.typicode.com/posts", {
	method: "POST",
	headers: {
		"Content-type": "application/json"
	},
	body: JSON.stringify({
		title: "New post",
		body: "Hellow World",
		userId: 1
	})
}).then(response => response.json())
	.then(json => console.log(json));

// Updating a post
// Updates a specific post following the Rest API (update, /posts/:id, PUT)

fetch("https://jsonplaceholder.typicode.com/posts/1", {
	method: "PUT",
	headers: {
		"Content-type": "application/json"
	},
	body: JSON.stringify({
		id: 1,
		title: "New post using PUT",
		body: "PUT method",
		userId: 1
	})
}).then(response => response.json())
	.then(json => console.log(json));

// Updates a specific post following the Rest API (update, /posts/:id, Patch)
// The difference between PUT and PATCH is the number of properties being changed
// PATCH is used to update the whole object
// PUT is used to update a single/several properties
fetch("https://jsonplaceholder.typicode.com/posts/1", {
	method: "PATCH",
	headers: {
		"Content-type": "application/json"
	},
	body: JSON.stringify({
		title: "Corrected post",
	})
}).then(response => response.json())
	.then(json => console.log(json));

// Deleting a post
// Deleting a specific post following the Rest API (delete, /posts/:id, DELETE)
fetch("https://jsonplaceholder.typicode.com/posts/1", {
	method: "DELETE"
	
});