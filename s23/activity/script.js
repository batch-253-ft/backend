// console.log("Heyy");
// console.log("Hello World");

//Follow the property names and spelling given in the google slide instructions.

// Create an object called trainer using object literals

// Initialize/add the given object properties and methods

// Properties

// Methods

// Check if all properties and methods were properly added


// Access object properties using dot notation

// Access object properties using square bracket notation

// Access the trainer "talk" method


// Create a constructor function called Pokemon for creating a pokemon


// Create/instantiate a new pokemon


// Create/instantiate a new pokemon


// Create/instantiate a new pokemon


// Invoke the tackle method and target a different object


// Invoke the tackle method and target a different object

let trainer = {
	name: "Ash Ketchum",
	age: 10,
	pokemon: ["Pikachu", "Charizard", "Squirtle", "Bulbasaur"],
	friends: {
		hoenn: ["May", "Max"],
		kanto: ["Brock", "Misty"],
	},
	talk: function(){
		return this.pokemon[0] + "! I choose you!";
	}

}

console.log(trainer);
console.log("Result of using dot notation method.");
console.log(trainer.name);
console.log("Result of using square bracket notation method.");
console.log(trainer["pokemon"]);
console.log("Result of using talk method.");
console.log(trainer.talk());



function Pokemon(name, level){

	this.name = name;
	this.level = level;
	this.health = 2 * level;
	this.attack = level;

	this.tackle = function (target){
		
		console.log(this.name + " tackled " + target.name);
		target.health = target.health - this.attack;
		console.log(target.name + "'s health is now reduced to " + target.health);
		
		if (target.health <=0 ){
			target.faint();
			}
		}
	this.faint = function(){
		console.log(this.name + " fainted.");	

		}


}

let pikachu = new Pokemon("Pikachu", 12);
let geodude = new Pokemon("Geodude", 8);
let mewtwo = new Pokemon("Mewtwo", 100);
console.log(pikachu);
console.log(geodude);
console.log(mewtwo);

geodude.tackle(pikachu);
console.log(pikachu);
mewtwo.tackle(geodude);
console.log(geodude)



//Do not modify
//For exporting to test.js
// try{
// 	module.exports = {
// 		trainer,
// 		Pokemon 
// 	}
// } catch(err) {

// }