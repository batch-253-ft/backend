const mongoose = require("mongoose");

const orderSchema = new mongoose.Schema(
  {
    userId: { 
        type: String, 
        required: [true, "User ID is required."] 
    },
    products: [
      {
        productId: {
          type: String,
        },
        quantity: {
          type: Number,
          default: 1,
        },
      },
    ],
    totalAmount: { 
        type: Number, 
        required: true
    }

  },
  { timestamps: true }
);

module.exports = mongoose.model("Order", orderSchema);