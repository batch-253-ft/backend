const mongoose = require("mongoose");

const cartSchema = new mongoose.Schema(
  {
    userId: {
      type: String,
  },
  products: [{
      productId: {
          type: String,
      },
      productName: {
        type: String,
    },
      quantity: {
          type: Number,
          required: true,
          min: [1, 'Quantity can not be less then 1.'],
          default: 1
      },
      price: {
        type: Number,
    },
      subtotal: {
        type: Number,
  
      }
  }],
  amount: {
      type: Number,
      required: true,
      default: 0
  },
  createdOn: {
    type: Date,
    default: new Date()
  }
});

module.exports = mongoose.model("Cart", cartSchema);
