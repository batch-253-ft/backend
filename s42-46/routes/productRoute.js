const express = require("express");
const router = express.Router();

const productController = require("../controllers/productController");
const auth = require("../auth");

// =============== Create Product (authorized)=================
router.post("/create", auth.verify, (req, res) => {
    const userData = auth.decode(req.headers.authorization);
    

    if(userData.isAdmin){
        productController.createProduct(req.body)
        .then(resultFromController => res.send(resultFromController))
        .catch(err => res.send(err))
    } else {
        res.send("Must be an Admin to create a product.");
    }
});

// ==================== Get All Products =====================
router.get("/allProducts", auth.verify, (req, res)=> {
    const userData = auth.decode(req.headers.authorization);
    
    if(userData.isAdmin){
    productController.getAllProducts()
    .then(resultFromController => res.send(resultFromController))
    .catch(err => res.send(err))
    }
})

// ==================== Get Active Products =====================
router.get("/activeProducts", (req, res)=> {
    productController.getActiveProducts()
    .then(resultFromController => res.send(resultFromController))
    .catch(err => res.send(err))
})


// ==================== Get Single Product by Id =====================
router.get("/:id", (req, res) => {
    productController.getSingleProduct(req.params.id)
    .then(resultFromController => res.send(resultFromController))
    .catch(err => res.send(err))
});

// ==================== Update Product(Admin Only) =====================
router.put("/:id", auth.verify, (req, res) => {
    const userData = auth.decode(req.headers.authorization);

    if(userData.isAdmin) {
        productController.updateProductInfo(req.params, req.body)
        .then(resultFromController => res.send(resultFromController))
        .catch(err => res.send(err))
    } else {
        res.send("Must be an admin to update product");
    };
});

// ================== Archive Product (Admin Only) ================
router.patch("/:producId/archive", auth.verify, (req, res) =>{
    const userData = auth.decode(req.headers.authorization);

    if(userData.isAdmin) {
        productController.archiveProduct(req.params, req.body).then(resultFromController => res.send(resultFromController)).catch(err => res.send(err));
    } else {
        res.send("Must be an admin to archive product");
    };
})

router.patch("/:producId/activate", auth.verify, (req, res) =>{
    const userData = auth.decode(req.headers.authorization);

    if(userData.isAdmin) {
        productController.activateProduct(req.params, req.body).then(resultFromController => res.send(resultFromController)).catch(err => res.send(err));
    } else {
        res.send("Must be an admin to activate product");
    };
})

module.exports = router;