const express = require("express");
const router = express.Router();

const userController = require("../controllers/userController");
const auth = require("../auth");

// ============ Register User ==============
router.post("/register", (req, res) => {
    userController.userRegistration(req.body)
    .then(resultFromController => res.send(resultFromController))
    .catch(err => res.send(err))
});

// ================== Login User ================

router.post("/login", (req, res) => {
    userController.loginUser(req.body)
    .then(resultFromController => res.send(resultFromController))
    .catch(err => res.send(err));
});

// =============== Get All Users (Admin only) ===================
router.get("/allUser", auth.verify, (req, res) => {
    const userData = auth.decode(req.headers.authorization);
    if(userData.isAdmin) {
        userController.getAllUsers({ userId : userData.id})
        .then(resultFromController => res.send(resultFromController))
        .catch(err => res.send(err))
    } else {
        res.send("Must be an Admin to get all users.");
    }
});
// =============== Get Single User ===================
router.get("/:userId/userDetails", auth.verify, (req, res) => {
    const userData = auth.decode(req.headers.authorization);
    
        userController.getSingleUser({ userId : userData.id})
        .then(resultFromController => res.send(resultFromController))
        .catch(err => res.send(err))
   
});

// ==================== Update User role (admin only) =====================
router.patch("/:userId/updateUserRole", auth.verify, (req, res) =>{
    const userData = auth.decode(req.headers.authorization);

    if(userData.isAdmin) {
        userController.updateUserRole(req.params, req.body).then(resultFromController => res.send(resultFromController)).catch(err => res.send(err));
    } else {
        res.send(false);
    };
})

router.post("/createOrder", auth.verify, (req, res) => {
    const userData = auth.decode(req.headers.authorization);
    let data = {
        userId : userData.id,
        productId: req.body.productId,
        productName: req.body.productName,
        price: req.body.price,
        quantity: req.body.quantity,
        subtotal: req.body.quantity*req.body.price
    }
    if(!userData.isAdmin) {
        userController.createOrder(data).then(resultFromController => res.send(resultFromController)).catch(err => res.send(err));

    } else {
        res.send(false);
    }

});
// =================== View user Cart =========================
router.get("/:userId/myCart", auth.verify, (req, res) => {
    const userData = auth.decode(req.headers.authorization);
    let data = userData.id;
    if(!userData.isAdmin) {
        userController.viewMyCart(data).then(resultFromController => res.send(resultFromController)).catch(err => res.send(err));
    } else {
        res.send(false);
    }
});

// ================= Add item to cart =============
router.post("/addItemToCart", auth.verify, (req, res) => {
    const userData = auth.decode(req.headers.authorization);
    
    if(!userData.isAdmin) {
        userController.updateUserRole(req.params, req.body).then(resultFromController => res.send(resultFromController)).catch(err => res.send(err));
    } else {
        res.send(false);
    };
})



module.exports = router;