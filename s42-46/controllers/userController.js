const bcrypt = require("bcrypt");
const auth = require("../auth");

const User = require("../models/User");
const Order = require("../models/Order");
const Product = require("../models/Product");
const Cart = require("../models/Cart");


// =============== User Registration ============
module.exports.userRegistration = (reqBody) => {
    return User.findOne({ email : reqBody.email}).then(result => {
        if(result){
            return "Email is already registered.";
        } else {
            let newUser = new User({
                firstName: reqBody.firstName,
                lastName: reqBody.lastName,
                email: reqBody.email,
                password: bcrypt.hashSync(reqBody.password, 10)
            });

            return newUser.save().then(user => {

                if(user){
                    return "New User has been registered."
                } else {
                    return false
                }
            }).catch(err => err)  
        }
    }).catch(err => err)
};

// =================== LOGIN USER =======================
module.exports.loginUser = (reqBody) => {
    
    return User.findOne({ email : reqBody.email}).then(result => {
       
        if(result == null){
            return "Email is not registered.";
        } else {
            const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password);
            if(isPasswordCorrect){
                return { access : auth.createAccessToken(result) }
            } else {
                return "Incorrect Password."
            }
        }
    }).catch(err => err);
}

// =============== Get All Users ==================
module.exports.getAllUsers = (data) => {
    return User.find({}).then(result => {
        return result;
    }).catch(err => err);
};

// =============== Get Single User ==================
module.exports.getSingleUser = (data) => {
    return User.findById(data.userId).then(result => {
        result.password = "";
        return result;
    }).catch(err => err);
};

// ================= Update User Role (admin only) ==================
module.exports.updateUserRole = (reqParams, reqBody) => {
    let updatedUserRole = {
        isAdmin: reqBody.isAdmin,   
    };
    return User.findByIdAndUpdate(reqParams.userId, updatedUserRole)
    .then(result=> true)
    .catch(err => err);
};

// ================= Create Order (user only) ==================
// module.exports.createOrder = async (data) => {
//     let {userId, productId, productName, price, quantity} = data;
//     let isUserUpdated = await User.findById(userId)
//         .then(user => {

//             user.orderedProducts.push({ 
//                 products : [{
//                     productId: productId,
//                     productName: productName,
//                     price: price,
//                     quantity: quantity
//                 }],
//                 totalAmount: quantity*price
//             });

//             return user.save().then(user => true)
//                 .catch(err => false)
//         });
    
//     let isProductUpdated = await Product.findById(productId)
//         .then(product => {

//             product.userOrders.push({ userId : userId});

//             return product.save().then(product => true)
//                 .catch(err => err)
//         });
    
//     if(isUserUpdated && isProductUpdated){
//         return true;
//     } else {
//         return false;
//     }    
// };

module.exports.createOrder = (data) => {
    let {userId, productId, productName, price, quantity, subtotal} = data;
    // check if cart is existing
    return Cart.findOne({userId: userId})
        .then(result => {
            if(!result){
                let newCart = new Cart({
                    userId : userId,
                    products: [{
                        productId : productId,
                        productName : productName,
                        price : price,
                        quantity : quantity,
                        subtotal : subtotal
                    }],
                    amount : subtotal
                })
                return newCart.save().then(cart => {
                    if(cart){
                        return true
                    } else {
                        return false
                    }
                }).catch(err => err)
            } else {
                return false

            }
        })
  
};


// ================ View My Cart ===================
module.exports.viewMyCart = (data) => {
    return Cart.findOne(data.userId).then(result => {
        return result;
    }).catch(err => err);
};

//================== add item to cart ===================
module.exports.addItemToCart = async (reqBody) => {
    let {cartId, productId, productName, price, quantity} = reqBody;

    let isCartUpdated = await Cart.findById(cartId)
        .then(userCart => {

            userCart.products.push({ 
                productId : productId,
                productName : productName,
                price : price,
                quantity : quantity,
                subtotal : price*quantity
            })

            return userCart.save().then(userCart => true)
                .catch(err => false)
        }).catch(err => err)
    if(isCartUpdated){
        amount += subtotal;
        let newAmount = {
            amount: amount 
        }
        return Cart.findByIdAndUpdate(cartId, newAmount)
        .then(result=> true)
        .catch(err => err);
            
    }
    
    

};




