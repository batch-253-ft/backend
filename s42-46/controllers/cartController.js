const express = require("express");
const router = express.Router();

const bcrypt = require("bcrypt");
const auth = require("../auth");

const User = require("../models/User");
const Order = require("../models/Order");
const Cart = require("../models/Cart");
const Product = require("../models/Product");

module.exports.addToCartProduct = async (req, res) => {
    const userId = req.params.id;
    const { productId, quantity } = req.body;
    
    try{ 
        let cart = await Cart.findById({userId: userId});
        let product = await Product.findById({id: productId});
        console.log("cart:", cart);
        console.log("product:", product);
        if(!product){
           return res.status(404).send("Product not found");
        }
        const price = product.price;
        const productName = product.productName;

        if(cart){
            // if cart exists for the user
            let productIndex = cart.product.findIndex(p => p.productId == productId);

            // Check if product exists or not
            if(productIndex > -1)
            {
                let productValue = cart.product[productIndex];
                productValue.quantity += quantity;
                cart.products[productIndex] = productValue;
            }
            else {
                cart.products.push({ productId, productName, quantity, price });
            }
            cart.amount += quantity*price;
            cart = await cart.save();
            return res.send(cart);
        }
        else{
            // no cart exists, create one
            const newCart = await Cart.create({
                userId,
                products: [{ productId, productName, price, quantity }],
                amount: quantity*price
            });
            return res.send(newCart);
        } 
        
    
    }
    catch (err) {
        console.log(err);
        res.status(500).send("Something went wrong");
    }


}
