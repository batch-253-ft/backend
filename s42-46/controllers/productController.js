const bcrypt = require("bcrypt");
const auth = require("../auth");

const Product = require("../models/Product");

// ================= Input New Product ==============
module.exports.createProduct = (reqBody) => {

    return Product.findOne({ productName : reqBody.productName}).then(result => {
        if(result){
            return "Duplicate found on the product list."
        } else {
            let newProduct = new Product({
                productName: reqBody.productName,
                description: reqBody.description,
                price: reqBody.price
            });
            return newProduct.save().then(product => {
                if(product){
                    return ("New product is now registered.")
                } else {
                    return false
                }
            }).catch(err => err)

        }
    })
};

// ================== Get All Product ==============
module.exports.getAllProducts = (reqBody) => {
    return Product.find({}).then(result => {
        return result;
    }).catch(err => err);
};

// ================== Get Active Products ==============
module.exports.getActiveProducts = (reqBody) => {
    return Product.find({isActive : true}).then(result => {
        return result;
    }).catch(err => err);
};

// ================== Get Single Product ==============
module.exports.getSingleProduct = (productId) => {
    return Product.findById(productId).then(result => {
        return result;
    }).catch(err => err);
};

// ================= Update Product (Admin only) ============
module.exports.updateProductInfo = (reqParams, reqBody) => {
	let updatedProduct = {
        productName: reqBody.prodctName,
        description: reqBody.description,
        price: reqBody.price
    };

    return Product.findByIdAndUpdate(reqParams.producId, updatedProduct)
    .then(product => "Product is now updated.")
    .catch(err => err);
};

//=============== Archive Product (Admin Only) ==============
module.exports.archiveProduct = (reqParams, reqBody) => {
    let archivedProduct = {
        isActive: false,   
    };
    return Product.findByIdAndUpdate(reqParams.productId, archivedProduct)
    .then(course => "Product status has been archived.")
    .catch(err => err);
};

//=============== Activate Product (Admin Only) ==============
module.exports.activateProduct = (reqParams, reqBody) => {
    let activatedProduct = {
        isActive: true,   
    };
    return Product.findByIdAndUpdate(reqParams.productId, activatedProduct)
    .then(course => "Product status has been activated.")
    .catch(err => err);
};




