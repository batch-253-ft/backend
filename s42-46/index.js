const express = require("express");
const mongoose = require("mongoose");

const cors = require("cors");

const userRoute = require("./routes/userRoute");
const productRoute = require("./routes/productRoute");
const cartRoute = require("./routes/cartRoute");

const app = express();

const port = process.env.PORT || 4000;
const db = mongoose.connection;

mongoose.connect("mongodb+srv://admin:admin123@b253-hernandez.zmuemci.mongodb.net/s42-46?retryWrites=true&w=majority", {
	useNewUrlParser: true,
	useUnifiedTopology: true
})

db.once("open", ()=> console.log("You are now connected to MongoDB Atlas. Yahoo!"));



app.use(express.json());
app.use(express.urlencoded({extended: true}));


app.use("/users", userRoute);
app.use("/products", productRoute);
app.use("/cart", cartRoute);


if(require.main === module){
	app.listen(port, () => {
		console.log(`API is now online on port http://localhost/${port} Yehey!`)
	});
}

module.exports = app;